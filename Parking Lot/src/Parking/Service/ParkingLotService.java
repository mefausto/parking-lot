package Parking.Service;

import Client.Entity.Vehicle;
import Parking.Entity.Comparator.FloorComparator;
import Parking.Entity.Floor;
import Parking.Entity.ParkingLot;
import Parking.Exception.InvalidParkingSystemException;

import java.util.Arrays;
import java.util.Optional;

public class ParkingLotService {
    private volatile static ParkingLotService instance;

    private final ParkingLot parkingLot;

    private ParkingLotService() {
        this.parkingLot = ParkingLot.getInstance();
    }

    public static ParkingLotService getInstance() {
        if (instance == null) {
            synchronized (ParkingLotService.class) {
                if (instance == null) {
                    instance = new ParkingLotService();
                }
            }
        }
        return instance;
    }

    public Optional<Floor> getBestSuitableFloor(Vehicle vehicle) {
        return Arrays.stream(parkingLot.getFloors())
                .filter(floor -> floor.isVehicleValidForFloor(vehicle))
                .min(FloorComparator.get());
    }


}
