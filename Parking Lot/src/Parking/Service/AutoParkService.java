package Parking.Service;

import Client.Entity.Vehicle;
import Parking.Entity.Floor;
import Parking.Exception.FloorIsNotSuitableException;
import ParkingApi.ParkServiceApi;
import Payment.Exception.CurrencyException;
import Payment.Service.PaymentService;

public class AutoParkService implements ParkServiceApi {
    private volatile static AutoParkService instance;

    private final ParkingLotService parkingLotService;
    private final PaymentService paymentService;

    private static final boolean SUCCESS = true, FAILURE = false;

    private AutoParkService() {
        this.parkingLotService = ParkingLotService.getInstance();
        this.paymentService = PaymentService.getInstance();
    }

    public static AutoParkService getInstance() {
        if (instance == null) {
            synchronized (AutoParkService.class) {
                if (instance == null) {
                    instance = new AutoParkService();
                }
            }
        }
        return instance;
    }

    @Override
    public boolean park(Vehicle vehicle) {
        try {
            Floor bestSuitableFloor = parkingLotService.getBestSuitableFloor(vehicle)
                    .orElseThrow(FloorIsNotSuitableException::new);
            bestSuitableFloor.parkVehicle(vehicle);
        } catch (FloorIsNotSuitableException e) {
            System.out.println("Vehicle can not park because of no suitable place");
            return FAILURE;
        }
        return SUCCESS;
    }

    @Override
    public boolean exit(Vehicle vehicle) throws CurrencyException {
        Floor vehicleFloor = vehicle.getParkFloor();
        paymentService.payFee(vehicle);
        vehicleFloor.exitVehicle(vehicle);
        return SUCCESS;
    }
}
