package Parking.Entity.Comparator;

import Parking.Entity.Floor;

import java.util.Comparator;

public class FloorComparator {
    public static Comparator<Floor> get() {
        return Comparator.comparing(Floor::getHeight).thenComparing(Floor::getAvailableWeight, Comparator.reverseOrder());
    }
}
