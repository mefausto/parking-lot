package Parking.Entity;

import Client.Entity.Vehicle;
import Parking.Exception.FloorIsNotSuitableException;

import java.util.ArrayList;
import java.util.List;

public final class Floor {
    private static final double MAX_HEIGHT = 5, MIN_HEIGHT = 2;
    private static final double MAX_TONNE = 100;
    private final double height;
    private final double weightCapacity;
    private double currentWeight;
    private final List<Vehicle> floorVehicles = new ArrayList<>();

    Floor() {
        height = MIN_HEIGHT + Math.random() * (MAX_HEIGHT - MIN_HEIGHT);
        weightCapacity = Math.random() * MAX_TONNE;
    }

    public final void parkVehicle(Vehicle vehicle) throws FloorIsNotSuitableException {
        if (!isVehicleValidForFloor(vehicle)) throw new FloorIsNotSuitableException();
        currentWeight = currentWeight + vehicle.getWeight();
        setupVehicleInfo(vehicle);
        floorVehicles.add(vehicle);
    }

    private void setupVehicleInfo(Vehicle vehicle) {
        vehicle.setParkTimeStamp();
        vehicle.setParkFloor(this);
    }

    public final void exitVehicle(Vehicle vehicle) {
        currentWeight = currentWeight - vehicle.getWeight();
        floorVehicles.remove(vehicle);
    }

    public boolean isVehicleValidForFloor(Vehicle vehicle) {
        return checkHeight(vehicle.getHeight()) && checkWeight(vehicle.getWeight());
    }

    private boolean checkHeight(double height) {
        return height < this.height;
    }

    private boolean checkWeight(double weight) {
        return currentWeight + weight < this.weightCapacity;
    }

    public double getHeight() {
        return height;
    }

    public double getAvailableWeight() {
        return weightCapacity - currentWeight;
    }

    @Override
    public String toString() {
        return String.format("Height %.2f ||| Current Weight %.2f ||| Capacity %.2f", height, currentWeight, weightCapacity);
    }
}
