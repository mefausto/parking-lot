package Parking.Entity;

import Parking.Exception.InvalidParkingSystemException;
import Parking.Service.ParkingLotService;

import java.util.Arrays;

public final class ParkingLot {
    private final Floor[] parkFloors;
    private volatile static ParkingLot instance;

    private ParkingLot(int numberOfFloor) {
        parkFloors = new Floor[numberOfFloor];
        setupFloors();
    }

    private void setupFloors() {
        for (int i = 0; i < parkFloors.length; i++)
            parkFloors[i] = new Floor();
    }

    public Floor[] getFloors() {
        return Arrays.copyOf(parkFloors, parkFloors.length);
    }

    public static ParkingLot getInstance(int numberOfFloor) {
        if (instance == null) {
            synchronized (ParkingLot.class) {
                if (instance == null) {
                    instance = new ParkingLot(numberOfFloor);
                }
            }
        }
        return instance;
    }

    public static ParkingLot getInstance() {
        if (instance == null) {
            throw new InvalidParkingSystemException();
        }
        return instance;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ParkingLot");
        builder.append(System.getProperty("line.separator"));
        builder.append("-------------------------------------------------------------------------");
        builder.append(System.getProperty("line.separator"));
        for (int i = 0; i < parkFloors.length; i++) {
            builder.append("| Park Floor:").append(i).append(" ");
            builder.append(parkFloors[i]);
            builder.append(System.getProperty("line.separator"));
        }
        builder.append("-------------------------------------------------------------------------");
        return builder.toString();
    }
}
