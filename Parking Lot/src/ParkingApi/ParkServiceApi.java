package ParkingApi;

import Client.Entity.Vehicle;
import Payment.Exception.CurrencyException;

public interface ParkServiceApi {
    boolean park(Vehicle vehicle);
    boolean exit(Vehicle vehicle) throws CurrencyException;
}
