package Client.Entity;

import Client.Exception.InvalidVehicleException;
import Parking.Entity.Floor;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public abstract class Vehicle implements Chargeable {
    private final static double MIN_WEIGHT = 0.0, MIN_HEIGHT = 0.0;
    protected double weight;
    protected double height;
    private LocalDateTime parkTimeStamp;
    private Floor parkFloor;

    public Vehicle(double weight, double height) throws InvalidVehicleException {
        isValidVehicle(weight, height);
        this.weight = weight;
        this.height = height;
    }

    private void isValidVehicle(double weight, double height) throws InvalidVehicleException {
        if (weight < MIN_WEIGHT || height < MIN_HEIGHT)
            throw new InvalidVehicleException();
    }

    protected BigDecimal parkDuration() {
        return new BigDecimal(ChronoUnit.MINUTES.between(parkTimeStamp, LocalDateTime.now()));
    }

    public double getWeight() {
        return weight;
    }

    public double getHeight() {
        return height;
    }

    public void setParkTimeStamp() {
        this.parkTimeStamp = LocalDateTime.now();
    }

    public Floor getParkFloor() {
        return parkFloor;
    }

    public void setParkFloor(Floor floor) {
        this.parkFloor = floor;
    }

    @Override
    public String toString() {
        return String.format("Vehicle{weight= %.2f, height=%.2f, parkTimeStamp="+parkTimeStamp+", parkFloor="+parkFloor+"}",weight, height);
    }
}
