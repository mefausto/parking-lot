package Client.Entity;

import Payment.Exception.CurrencyException;
import Payment.Entity.Money;

import java.math.BigDecimal;

public interface Chargeable {
    BigDecimal DEFAULT_PRICE = new BigDecimal(100L);
    Money getFee() throws CurrencyException;

}
