package Client.Entity;

import Client.Exception.InvalidVehicleException;
import Payment.Entity.Money;
import Payment.Exception.CurrencyException;

import java.math.BigDecimal;

public class Car extends Vehicle {
    public Car(double weight, double height) throws InvalidVehicleException {
        super(weight, height);
    }

    @Override
    public Money getFee() throws CurrencyException {
        return Money.of(DEFAULT_PRICE.add(BigDecimal.TEN.multiply(parkDuration())));
    }
}
