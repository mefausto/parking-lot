import Client.Entity.Car;
import Client.Entity.Vehicle;
import Client.Exception.InvalidVehicleException;
import Parking.Entity.ParkingLot;
import Parking.Service.AutoParkService;
import ParkingApi.ParkServiceApi;
import Payment.Exception.CurrencyException;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String... args) throws InvalidVehicleException, CurrencyException {
        //argument zero is going to be our number of floor.
        int numberOfFloor = Integer.parseInt(args[0]);
        int generatedVehicles = Integer.parseInt(args[1]);
        //we have only one parking lot that's why it is going to be singleton.
        ParkingLot parkingLot = ParkingLot.getInstance(numberOfFloor);
        ParkServiceApi autoParkService = AutoParkService.getInstance();
        List<Vehicle> vehicles = new ArrayList<>();
        //Generate cars and park
        for(int i=0; i<generatedVehicles; i++){
            double height = 2 + Math.random() * (3);
            double weight = Math.random() * 50;
            Car car = new Car(weight,height);
            boolean isSuccess = autoParkService.park(car);
            System.out.println(car);
            System.out.println(parkingLot);
            if(isSuccess) vehicles.add(car);
        }
        //exit cars
        for(Vehicle vehicle: vehicles)
            autoParkService.exit(vehicle);
    }
}
