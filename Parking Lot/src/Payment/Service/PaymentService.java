package Payment.Service;

import Client.Entity.Chargeable;
import Payment.Exception.CurrencyException;

public final class PaymentService {

    private volatile static PaymentService instance;

    public final void payFee(Chargeable vehicle) throws CurrencyException {
        System.out.println("Payment Success "+ vehicle.getFee());
    }

    public static PaymentService getInstance() {
        if (instance == null) {
            synchronized (PaymentService.class) {
                if (instance == null) {
                    instance = new PaymentService();
                }
            }
        }
        return instance;
    }

}
