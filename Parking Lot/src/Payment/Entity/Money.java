package Payment.Entity;

import Payment.Exception.CurrencyException;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Optional;

public final class Money {
    private final BigDecimal amount;

    public enum Currency {
        PENNY;

        static boolean isExist(Currency currency) {
            return Arrays.stream(Currency.values()).anyMatch(supportedCurrency -> currency == supportedCurrency);
        }
    }

    public Money(BigDecimal amount, Currency currency) throws CurrencyException {
        Optional.of(Money.Currency.isExist(currency)).orElseThrow(CurrencyException::new);
        this.amount = amount;
    }

    public static Money of(BigDecimal amount) throws CurrencyException {
        return new Money(amount, Currency.PENNY);
    }

    @Override
    public String toString() {
        return "Money{" +
                "amount=" + amount +
                '}';
    }
}
